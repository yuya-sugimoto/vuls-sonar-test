<?php
include_once "C:/xampp_5.6/htdocs/vuls-sonar-test/app/Core/View.php";

class FormCompleteController
{
    private $form_name = "/form-complete";
    private $request = [];

    /***
     * XSS 脆弱性演習
     * @param $_get $_GET
     * @param $_post $_POST
     */
    function __construct($_get, $_post) {

        $this->request['get'] = $_get;
        $this->request['post'] = $_post;
    }

    function index() {

        View::rendering($this->form_name . '/index.php', $this->request);
    }

    function confirm() {}

    function finish() {}
}