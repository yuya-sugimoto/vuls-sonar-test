<?php


class View
{
    private static $view_path = "C:/xampp_5.6/htdocs/vuls-sonar-test/view";
    
    static function rendering(string $view_name, array $data) {

        ob_start();
        extract($data);

        include(self::$view_path . $view_name);

        $html = ob_get_clean();

        echo $html;
    }
}